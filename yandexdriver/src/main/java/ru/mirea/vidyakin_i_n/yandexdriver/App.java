package ru.mirea.vidyakin_i_n.yandexdriver;


import android.app.Application;

import com.yandex.mapkit.MapKitFactory;

public class App extends Application {
    private final String MAPKIT_API_KEY = "8514ddc7-01a3-4ca4-b7d7-246dce36f27a";
    @Override
    public void onCreate() {
        super.onCreate();
        MapKitFactory.setApiKey(MAPKIT_API_KEY);
    }
}